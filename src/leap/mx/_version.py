
# This file was generated by the `freeze_debianver` command in setup.py
# Using 'versioneer.py' (0.7+) from
# revision-control system data, or from the parent directory name of an
# unpacked source archive. Distribution tarballs contain a pre-generated copy
# of this file.

version_version = '0.7.0'
version_full = '161e72a01ec53a6dae7fe5b4229b478377ca6458'


def get_versions(default={}, verbose=False):
        return {'version': version_version, 'full': version_full}
